#include <ncurses.h>

int main(int argc, char *argv[])
{
    // Initializes the screen,
    // sets up memory and clean the screen.
    initscr();

    // Prints a string(const char *) to a window.
    printw("Hello, world!");

    // Refreshes the screen to match what is in memory.
    refresh();

    // waits for user input, returns the int value for that key.
    getch();

    // Dealocates memory and ends ncurses.
    endwin();

    return 0;
}