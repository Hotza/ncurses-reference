#include <stdio.h>
#include <ncurses.h>

int main (int argc, char *argv[])
{
  initscr();

  int y, x;
  char message[20];
  
  printw("Plese enter a value for Y: ");
  scanw("%d", &y);

  clear();

  printw("Plese enter a value for X: ");
  scanw("%d", &x);

  clear();

  printw("Enter a message: ");
  scanw("%s", message);

  clear();

  mvprintw(y, x, message);

  getch();

  endwin();

  return 0;
}
